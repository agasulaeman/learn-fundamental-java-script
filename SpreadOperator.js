/**
 * Spread Operator
 *
 * Masih terkait dengan array, ES6 memiliki fitur menarik untuk membantu pengelolaan array menjadi lebih mudah, yaitu spread operator.
 *
 * Sesuai namanya “spread”,
 * fitur ini digunakan untuk menyebarkan nilai array atau lebih tepatnya iterable object menjadi beberapa elemen.
 * Spread operator dituliskan dengan tiga titik (...). Mari kita lihat contoh kode berikut:
 */

const favorites = ["Seafood", "Salad", "Nugget", "Soup"];

console.log(favorites);
console.log(...favorites);
console.log(favorites[0], favorites[1], favorites[2], favorites[3]);

/**
 * Spread operator dapat digunakan untuk menggabungkan dua buah array ke dalam array baru.
 * Jika tidak menggunakan spread operator ini maka hasilnya akan seperti ini:
 */

const others = ["Cake", "Pie", "Donut"];

const allFavorites = [favorites, others];

console.log(allFavorites);

/**
 * Nilai array tidak akan tergabung. Alih-alih menggabungkan nilainya,
 * variabel allFavorites menjadi array baru yang menampung dua array di dalamnya.
 * Nah, lantas bagaimana jika kita mencoba menggunakan spread operator?
 */

const allFavorites1 = [...favorites, ...others];

console.log(allFavorites1);

/**
 * dengan menggunakan spread operator nilai dua array tersebut berhasil tergabung.
 *
 * Selain array, spread operator juga bisa digunakan untuk object literals.
 * Hal ini memungkinkan kita dapat menggabungkan beberapa object dengan kode yang lebih ringkas.
 */

const obj1 = { firstName: 'Obi-Wan', age: 30 };
const obj2 = { lastName: 'Kenobi', gender: 'M' };

const newObj = { ...obj1, ...obj2 };

console.log(newObj);