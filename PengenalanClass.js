/**
 * Class
 *
 * Class adalah hal yang sangat penting dalam pemrograman berorientasi objek.
 * Hal itu karena class menyediakan informasi tentang suatu object.
 * Jadi dapat dikatakan object adalah instance dari sebuah class.
 * Class sendiri dalam paradigma OOP secara teknis merupakan sebuah blueprint dalam mendefinisikan karakteristik dari
 * sebuah objek. Sebagai contoh, misalkan terdapat blueprint untuk mendefinisikan objek Mail.
 * Yang mana sms dan postman adalah object dari class Mail.
 *
 * Nama Class           Mail
 * Karakteristik        pengirim, penerima, isi pesan
 * Kapabilitas/aksi     kirim pesan, terima pesan
 *
 * Di dalam sebuah class, dapat terdiri dari properti dan method.
 * Properti merupakan karakteristik dari class,
 * sedangkan method adalah kapabilitas atau kemampuan yang dimiliki oleh class.
 *
 * Nah, untuk penulisan class di JavaScript sendiri bisa menggunakan dua cara,
 * yakni melalui sintaks function ataupun class.
 *
 * Mari kita lihat dulu cara membuat class menggunakan sintaksis function.
 *
 * Function Menggunakan pendekatan prototype
 *
 * function Mail() {
 *   this.from = 'pengirim@dicoding.com';
 * };
 *
 * Mail.prototype.sendMessage = function(msg, to) {
 *   console.log(`you send: ${msg} to ${to} from ${this.from}`);
 * };
 *
 * //pemanggilan
 * const mail1 = new Mail();
 * mail1.sendMessage('hallo', 'penerima@dicoding.com');
 *
 *
 * output:
 * you send: hallo to penerima@dicoding.com from pengirim@dicoding.com
 *
 * function tanpa menggunakan prototype :
 *
 * function Mail(){
 *   this.from = "pengirim@dicoding.com",
 *   this.sendMessage = function(msg, to) {
 *     console.log(`you send: ${msg} to ${to} from ${this.from}`);
 *   }
 * };
 *
 *
 */

function Mail() {
    this.from = "pengirim@dicoding.com";

};

Mail.prototype.sendMessage = function(msg, to) {
    console.log('tanpa prototype');
    console.log(`you send: ${msg} to ${to} from ${this.from}`);
};

//caller
const  pesan1 = new Mail();
pesan1.sendMessage('hai','agas@gmail.com')


//tanpa prototype
function Mail2(){
    this.from = "agas@dicoding.com",
        this.sendMessage = function(msg, to) {
        console.log('dengan prototype');
        console.log(`you send: ${msg} to ${to} from ${this.from}`);
        }
};
const  pesan2 = new Mail2();
pesan2.sendMessage('hai','agas@gmail.com');

/**
 * Ketika kita meng-instantiate objek-objek lain,
 * objek yang menggunakan prototype tidak meng-copy atribut sendMessage ke setiap objek-objek.
 * Berbeda ketika kita tidak menggunakan prototype,
 * semua attribute di-copy ke setiap objek. Dengan demikian,
 * penggunaan prototype dapat menghemat alokasi memori yang digunakan.
 *
 *
 */


/**
 * Lanjut ke cara kedua, yakni menggunakan sintaksis class.
 *
 * class Mail {
 *     constructor() {
 *         this.from = 'pengirim@dicoding.com';
 *     }
 *
 *     sendMessage(msg, to) {
 *         console.log(`you send: ${msg} to ${to} from ${this.from}`);
 *     };
 * }
 *
 * const mail1 = new Mail();
 * mail1.sendMessage('hallo', 'penerima@dicoding.com');
 */

class kendaraan {
    constructor(nama,typeKendaraan,year) {
    this.nama = nama;
    this.typeKendaraan = typeKendaraan;
    this.year = year;
    }
    usiaKendaraan () {
        let date = new Date();
        return date.getFullYear() - this.year
    }
}

const myCar = new kendaraan ("Honda Jazz","City car",2005);
console.log(myCar.nama + " " + myCar.typeKendaraan + " " + myCar.usiaKendaraan());

class People {
    constructor() {
        this.email = 'agas@gmail.com';
    }

    cariemailKaryawan() {
        console.log('result : ' + this.email);
    };
}
    const employee = new People ();
    employee.cariemailKaryawan();

