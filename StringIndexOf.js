const paragraph = 'The quick brown fox jumps over the lazy dog. If the dog barked, was it really lazy?';

const searchTerm = 'dog';
const indexOfFirst = paragraph.indexOf(searchTerm);
const totalParagraph = paragraph.length;

console.log(`The index of the first "${searchTerm}" from the beginning is ${indexOfFirst}`);
// expected output: "The index of the first "dog" from the beginning is 40"

console.log(`The index of the 2nd "${searchTerm}" is ${paragraph.indexOf(searchTerm, (indexOfFirst + 1))}`);
// expected output: "The index of the 2nd "dog" is 52"

console.log(totalParagraph);

/**
 *  if the value not in variable
 *  it will be return -1 (false)
 * @type {string}
 */

const kalimat = "Gangzar agas Sulaeman";
console.log(kalimat.indexOf('Ga'));

console.log(kalimat.slice(3))
console.log(kalimat.slice(-3))
console.log(kalimat.slice(3,10))

console.log(kalimat.replace('agas','dwi mawarni'))
console.log(kalimat.repeat(3))


/**
 * task section 15
 * @type {string}
 */

// DON'T TOUCH THIS LINE! (please)
const word = "skateboard"; //Don't change this line!

// YOUR CODE BELOW THIS LINE:

//console.log(word.slice(5))
//console.log(word.replace('o','e'))
let facialHair = word.slice(5).replace('o','e') /*word.replace('o','e');*/
console.log(facialHair)


