/**
 * Loop
 *
 * Ketika menulis program komputer, akan ada situasi di mana kita perlu melakukan hal yang sama berkali-kali.
 * Misalnya kita ingin menampilkan semua nama pengguna yang terdaftar di aplikasi atau
 * sesederhana menampilkan angka 1 sampai 10. Tentunya tidak praktis jika kita menulis kode seperti berikut:
 *
 * console.log(1);
 * console.log(2);
 * console.log(3);
 * console.log(4);
 * console.log(5);
 * console.log(6);
 * console.log(7);
 * console.log(8);
 * console.log(9);
 * console.log(10);
 *
 * base syntax
 *
 * for(let i = 0; i < 5; i++) {
 *     console.log(i);
 * }
 */


/**
 * contoh
 */

for(let i = 0; i < 5; i++) {
    console.log(i);
}


/**
 * For of loop
 *
 * Cara lain dalam melakukan looping adalah menggunakan for..of. For of mulai hadir pada ECMAScript 2015 (ES6).
 * Cara ini jauh lebih sederhana dan modern dibanding for loop biasa. Sintaks dasar dari for of loop adalah seperti ini:
 *
 * for(arrayItem of myArray) {
 *     // do something
 * }
 */


/**
 * contoh
 */

let myArray = ["Luke", "Han", "Chewbacca", "Leia"];

for(const arrayItem of myArray) {
    console.log(arrayItem)
}

let bahasaDiDunia = ['Inggris','Germany','France','Indonesia'];
for (const bahasaDalamArray of bahasaDiDunia) {
    console.log(bahasaDalamArray)
}

/**
 * While do while
 *
 * Metode lain untuk melakukan looping adalah dengan statement while. Sama seperti for,
 * instruksi while mengevaluasi ekspresi boolean dan menjalankan kode di dalam blok while ketika bernilai true.
 *
 * Untuk menampilkan angka 1 sampai 100 dengan while kita bisa menulis kode seperti berikut:
 */


/**
 * Contoh
 */

let i = 1;

while (i <= 100) {
    console.log(i);
    i++;
}

/**
 * dO - wHILE
 *
 * Bentuk lain dari while adalah perulangan do-while.
 */

console.log('=========================do-while=========================')

let nilai1 = 1;

do {
    console.log(nilai1);
    nilai1++;
} while (nilai1 <= 100);


/**
 * Infinte loop
 *
 * Ketika menerapkan perulangan pada program, ada satu kondisi yang perlu kita hindari yaitu infinite loop.
 * Infinite loop atau endless loop adalah kondisi di mana program kita stucked di dalam perulangan.
 * Ia akan berjalan terus hingga menyebabkan crash pada aplikasi dan komputer kecuali ada intervensi secara eksternal,
 * seperti mematikan aplikasi.
 *
 * Kode berikut ini adalah contoh di mana kondisi infinite loop dapat terjadi:
 */

/*//contoh dengan while
let i = 1;

while (i <= 5) {
    console.log(i);
}

//contoh dengan for

for(let i = 1; i <= 5; i=1) {
    console.log(i);
}*/

let nama ='agas';
console.log(typeof(nama));


