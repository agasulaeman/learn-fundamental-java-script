const firstString = "Hello";
const secondString = "JavaScript";

console.log(firstString + secondString);

if ((true || false) && (false || false)) {
    console.log("It's true");
} else {
    console.log("It's false");
}

let myVariable = 12;
myVariable = 30;
myVariable = 5;

console.log(myVariable);

const name = 'Dicoding';
const language = 'JavaScript';

console.log(`Hello $name. Welcome to $language!`);

