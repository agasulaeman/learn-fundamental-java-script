/**
 *
 * MAP
 *
 * Map adalah tipe data yang menyimpan koleksi data dengan format key-value layaknya Object.
 * Yang membedakan adalah Map memperbolehkan key dengan tipe data apa pun,
 * dibandingkan Object yang hanya mengizinkan key bertipe String atau Symbol.
 *
 * Untuk mendefinisikan Map gunakan constructor seperti di bawah ini:
 *
 * const myMap = new Map();
 */

/**
 * Apabila ingin menetapkan nilai dari Map secara langsung, gunakan array multi dimensi seperti ini:
 */

/*
const myMap = new Map([
    ['1', 'a String key'],
    [1, 'a number key'],
    [true, true]
]);

console.log(myMap);
*/

/**
 * Array pertama (yang berada di luar) berfungsi untuk menyimpan masing-masing elemen atau pasangan key-value dari Map.
 * Kemudian array di dalamnya, memiliki dua elemen,
 * di mana elemen pertama adalah key dan elemen keduanya merupakan value.
 *
 * Ketika sudah membuat objek Map,
 * kita bisa mendapatkan nilainya berdasarkan key tertentu dengan metode get().
 * Lalu, untuk menambahkan pasangan key-value baru gunakan metode set().
 */

/*
const capital = new Map([
    ["Jakarta", "Indonesia"],
    ["London", "England"],
    ["Tokyo", "Japan"]
]);
*/

/*
console.log(capital.size);
console.log(capital.get("London"));
capital.set("New Delhi", "India");
console.log(capital.size);
console.log(capital.get("New Delhi"));
*/

const namaDaerah = new Map(
  [
    [1,"Indonesia",true],
    [2,"England",false],
    [3,"Brunei Darussalam",true]

]);

console.log(namaDaerah.size);
console.log(namaDaerah.get("England"));
namaDaerah.set(4,"Australia",false);
console.log(namaDaerah.size);
console.log(namaDaerah.get(4));

/**
 * Meskipun kita bisa menetapkan dan mendapatkan data pada map seperti ini:
 *
 * const wrongMap = new Map();
 *
 * wrongMap["My Key"] = "My Value";
 * console.log(wrongMap["My Key"]);
 *
 * Namun, kode di atas bukanlah cara yang sesuai untuk mengoperasikan Map.
 * Ketika kita menetapkan nilai map seperti di atas,
 * data akan disimpan sebagai generic object.
 * Ini akan mengakibatkan data tidak tersimpan dalam Map query dan tidak bisa menggunakan fitur dari Map seperti
 * .has atau .delete.
 */

/**
 * Jadi, pastikan untuk menggunakan metode .set() dan .get() untuk Map.
 */