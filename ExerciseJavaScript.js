function sum(a, b) {
    return a + b;

}

console.log(sum(1, 3));
console.log(sum(2, 5));

function multiply(a, b) {
    return a * b
}

console.log(multiply(1, 3));
console.log(multiply(2, 5));

let text = "hello world";
console.log(text.length);
console.log(text.toUpperCase());

function tambah(a, b) {
    console.log(a);
    console.log(a + b);
    return a + b;
}

tambah(1, 3);

/**
 * @param {string} str
 */
function getCharCount(str) {

    return str.length;
}

// Sample usage - do not modify
console.log(getCharCount("Sam")); // 3
console.log(getCharCount("Alex 123")); // 8
console.log(getCharCount("Charley is here")); // 15

function shoutMyName(name) {
    return name.toUpperCase();
}

console.log(shoutMyName("Sam")); // "SAM"
console.log(shoutMyName("Charley")); // "CHARLEY"
console.log(shoutMyName("alex")); // "ALEX"


function lowerName(name) {
    return name.toLowerCase();
}

// Sample usage - do not modify
console.log(lowerName("Sam")); // "sam"
console.log(lowerName("ALEX")); // "alex"


/**
 * Character Accsess
 * @type {string}
 */

const language = "JavaScript";
console.log(language[6]);

console.log(language.length -3);

/**
 * The .at(index) method
 * Since 2022, JavaScript now has a .at() method that reads the character at a certain index, which can also be negative.
 *
 * Let's take a look at a few examples:
 */

//language.at(0);

function getFirstCharacter(name) {

    return name.substr(name.length - 1);

}

// Sample usage - do not modify
console.log(getFirstCharacter("Sam")); // "A"
console.log(getFirstCharacter("Alex")); // "J" Charley
console.log(getFirstCharacter("Charley"));