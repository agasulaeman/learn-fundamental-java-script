/**
 * cara ingin mengetahui sebuah tipe data
 * @type {string}
 */

let greet = "Hello";
console.log(typeof(greet))


let angka = 1;
console.log(typeof(angka))

let pecahan = 0.7;
console.log(typeof (pecahan))

let x = true;
let y = false;

console.log(typeof(x))
console.log(typeof(y))

/* output:
boolean
boolean
*/

const a = 10;
const b = 12;

let isGreater = a > b;
let isLess = a < b;

console.log(isGreater);
console.log(isLess);

/* output:
false
true
*/
