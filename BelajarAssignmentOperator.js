/**
 * Operator
 * Pada materi ini kita akan mempelajari tentang operator yang terdapat pada JavaScript.
 * Operator dalam bahasa pemrograman sendiri adalah simbol yang memberi tahu interpreter
 * untuk melakukan operasi seperti matematika, relasional, atau logika untuk memberikan hasil tertentu.
 */


/**
 * Assignment Operator
 * Dari contoh kode yang kita gunakan sebelumnya, sebenarnya kita sudah menggunakan assignment operator.
 * Operator ini digunakan untuk memberikan nilai pada variabel.
 *
 * Pada dasarnya operator ini adalah tanda sama dengan (=),
 * di mana tanda ini digunakan untuk menginisialisasi nilai pada variabel.
 * Tempatkan variabel yang ingin diberi nilai di sebelah kiri, sementara nilainya di sebelah kanan.
 * Di antara keduanya terdapat operator assignment.
 */

//Contoh

let x = 10;
let y = 5

x += y;

console.log(x);

let a = 10;
let b = 7;
a--;
console.log(a)

/**
 * let x = 10;
 * let y = 5;
 *
 * x += y; // artinya -> x = x + y;
 * x -= y; // artinya -> x = x - y;
 * x *= y; // artinya -> x = x * y;
 * x /= y; // artinya -> x = x / y;
 * x %= y; // artinya -> x = x % y;
 */

/**
 * Comparison Operator
 *
 * Terdapat serangkaian karakter khusus yang disebut dengan operator pembanding/komparasi yang dapat mengevaluasi dan
 * membandingkan dua nilai. Berikut daftar operator dan fungsinya:
 *
 * ==	Membandingkan kedua nilai apakah sama (tidak identik).
 *
 * !=	Membandingkan kedua nilai apakah tidak sama (tidak identik).
 *
 * ===	Membandingkan kedua nilai apakah identik.
 *
 * !==	Membandingkan kedua nilai apakah tidak identik.
 *
 * >	Membandingkan dua nilai apakah nilai pertama lebih dari nilai kedua.
 *
 * >=	Membandingkan dua nilai apakah nilai pertama lebih atau sama dengan nilai kedua.
 *
 * <	Membandingkan dua nilai apakah nilai pertama kurang dari nilai kedua.
 *
 * <=	Membandingkan dua nilai apakah nilai pertama kurang atau sama dengan nilai kedua.
 *
 *
 */

/**
 * cONTOH COMPARISON OPERATOR
 */


let nilaiA = 10;
let nilaiB = 12;

console.log(nilaiA < nilaiB);
console.log(nilaiA > nilaiB);

/**
 * Perbedaan antara “Sama” dan “Identik”
 * Dalam operator komparasi di JavaScript, hal yang menjadi sedikit “tricky” adalah membedakan antara “sama” (==) dan
 * “identik” (===).
 *
 * Kita sudah mengetahui bahwa setiap nilai pasti memiliki tipe data baik itu number, string atau boolean.
 * Contohnya sebuah string “10” dan number 10 merupakan hal yang serupa, tetapi keduanya tidak benar-benar sama.
 *
 * Hal inilah yang membedakan antara sama dan identik pada JavaScript. Jika kita ingin membandingkan hanya dari kesamaan nilainya kita bisa gunakan == tapi jika kita ingin membandingkan dengan memperhatikan tipe datanya kita gunakan ===.
 *
 * Contohnya seperti berikut:
 */


/**
 * Contoh perbedaan Sama dan identik
 */

const aString = '10';
const aNumber = 10

console.log( aString == aNumber) // true, karena nilainya sama-sama 10
console.log( aString === aNumber) // false, karena walaupun nilainya sama, tetapi tipe datanya berbeda


/**
 * Logical Operator
 *
 * Pada JavaScript terdapat tiga buah karakter khusus yang berfungsi sebagai logical operator.
 * Berikut macam-macam logical operator dan fungsinya:
 *
 * &&	Operator dan (and). Logika akan menghasilkan nilai true apabila semua kondisi terpenuhi (bernilai true).
 *
 * ||	Operator atau (or). Logika akan menghasilkan nilai true apabila ada salah satu kondisi terpenuhi (bernilai true).
 *
 * !
 * Operator tidak (not). Digunakan untuk membalikkan suatu kondisi.
 */

let logicalA = 10;
let logicalB = 12;

/* AND operator */
console.log(logicalA < 15 && logicalB > 10); // (true && true) -> true
console.log(logicalA > 15 && logicalB > 10); // (false && true) -> false

/* OR operator */
console.log(logicalA < 15 || logicalB > 10); // (true || true) -> true
console.log(logicalA > 15 || logicalB > 10); // (false || true) -> true

/* NOT operator */
console.log(!(logicalA < 15)); // !(true) -> false
console.log(!(logicalA < 15 && logicalB > 10)); // !(true && true) -> !(true) -> false

/* output
true
false
true
true
false
false
*/
