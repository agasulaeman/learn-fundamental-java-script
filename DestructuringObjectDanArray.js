/**
 *
 * Destructuring Object & Array
 *
 * Literasi object dan array adalah dua hal yang paling banyak digunakan dalam mengelola data di JavaScript.
 * JSON (JavaScript Object Notation) merupakan format data paling populer yang digunakan dalam transaksi data saat ini.
 *
 *
 */

/**
 *
 * [
 *     {
 *         "id": 14,
 *         "title": "Belajar Fundamental Aplikasi Android",
 *         "author": "Google ATP"
 *     },
 *     {
 *         "id": 51,
 *         "title": "Belajar Membuat Aplikasi Android untuk Pemula",
 *         "author": "Google ATP"
 *     },
 *     {
 *         "id": 123,
 *         "title": "Belajar Dasar Pemrograman Web",
 *         "author": "Dicoding Indonesia"
 *     },
 *     {
 *         "id": 163,
 *         "title": "Belajar Fundamental Front-End Web Development",
 *         "author": "Dicoding Indonesia"
 *     }
 * ]
 *
 */

/**
 *
 * Jika kita lihat pada struktur JSON di atas, kita dapat menyimpulkan struktur tersebut dibangun dari array dan object.
 * Karena kedua hal ini banyak digunakan untuk mengelola data pada JavaScript untuk memudahkan developer,
 * ES6 menambahkan fitur untuk destructuring object dan array.
 *
 * Apa sebenarnya destructuring object dan array itu?
 * Destructuring dalam JavaScript merupakan sintaksis yang dapat mengeluarkan nilai dari array atau properties
 * dari sebuah object ke dalam satuan yang lebih kecil.
 *
 * Secara tidak sadar mungkin kita pernah melakukan destructuring.
 * Namun, sebelum ES6 hal tersebut dilakukan dengan cara seperti ini:
 *
 */

const profile = {
    firstName: "John",
    lastName: "Doe",
    age: 18
}

const firstName = profile.firstName;
const lastName = profile.lastName;
const age = profile.age;

console.log(firstName,lastName,age);



/**
 *
 * Perhatikan kode di atas, kode tersebut mengekstraksi nilai yang berada di dalam objek,
 * kemudian menyimpannya pada variabel lokal dengan nama sama dengan properti di dalam object profile.
 * Mungkin mengekstraksi nilai dari object dengan langkah ini terlihat mudah,
 * tetapi bayangkan jika object memiliki banyak properti dan harus melakukan hal tersebut secara manual satu persatu.
 * Terlalu banyak kode yang dituliskan berulang, bukan?
 *
 * Itulah alasan ES6 menambahkan fitur yang memudahkan kita untuk destructuring object maupun array.
 * Ketika kita ingin memecah struktur data menjadi bagian-bagian yang lebih kecil,
 * kita akan dipermudah untuk mendapatkan data yang diinginkan.
 *
 * Lantas bagaimana cara melakukan destructuring object dan array pada ES6? Mari kita simak materi berikutnya.
 *
 */

/**
 * Destructuring Object
 *
 * Penulisan sintaksis destructuring object pada ES6 menggunakan object literal ({ }) di sisi kiri dari operator assignment.
 *
 * const profile = {
 *     firstName: "John",
 *     lastName: "Doe",
 *     age: 18
 * }
 *
 * const {firstName, lastName, age} = profile;
 *
 * console.log(firstName, lastName, age);
 *
 * output:
 * John Doe 18
 *
 */

/**
 * Saat melakukan destructuring assignment, kita perlu menuliskan destructuring object di dalam tanda kurung.
 * Jika tidak menuliskan tanda kurung, tanda kurung kurawal akan membuat JavaScript mengira kita membuat block statement,
 * sementara block statement tidak bisa berada pada sisi kiri assignment.
 *
 * CONTOH :
 *
 * // tidak bisa karena JavaScript mengira kita membuat block statement
 * // block statement tidak bisa berada pada sisi kiri assignment
 * {firstName, age} = profile;
 */

const profile1 = {
    firstName1: "John",
    lastName1: "Doe",
    age1: 18
}


const {firstName1, age1, isMale1} = profile1;

console.log(firstName1)
console.log(age1)
console.log(isMale1)

/* output:
John
18
undefined
*/
/**
 * Alternatifnya, kita bisa secara opsional mendefinisikan nilai default pada properti tertentu jika tidak ditemukan.
 * Untuk melakukanya, tambahkan tanda assignment (=) setelah nama variabel dan tentukan nilai default-nya seperti ini:
 *
 * const profile = {
 *     firstName: "John",
 *     lastName: "Doe",
 *     age: 18
 * }
 *
 *
 * const {firstName, age, isMale = false} = profile;
 *
 * console.log(firstName)
 * console.log(age)
 * console.log(isMale)
 *
 * output:
 * John
 * 18
 * false
 *
 */

/**
 * Assigning to Different Local Variable Names
 *
 * Sampai saat ini kita tahu bahwa untuk melakukan destrukturisasi object pada variabel lokal,
 * kita perlu menyeragamkan penamaan variabel lokal dengan properti object-nya. Namun,
 * sebenarnya dalam proses destrukturisasi object kita bisa menggunakan penamaan variabel lokal yang berbeda.
 * ES6 menyediakan sintaksis tambahan yang membuat kita dapat melakukan hal tersebut.
 * Penulisannya mirip seperti ketika kita membuat properti beserta nilainya pada object.
 *
 * Contohnya seperti ini:
 */

const biodata = {
    firstName: "Agas",
    lastName: "Sulaeman",
    age: 18
}

const {firstName: localFirstName, lastName: localLastName, age: localAge} = biodata;

console.log(localFirstName);
console.log(localLastName);
console.log(localAge);

