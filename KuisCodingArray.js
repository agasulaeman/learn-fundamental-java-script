/**
 * TODO:
 * Buatlah sebuah variabel dengan nama evenNumber yang merupakan sebuah array dengan ketentuan:
 *   - Array tersebut menampung bilangan genap dari 1 hingga 100
 *
 * Catatan:
 *   - Agar lebih mudah, gunakanlah for loop dan logika if untuk mengisi bilangan genap pada array.
 */

// TODO

const evenNumber = [];

for (let i = 1; i <= 100; i++) {
    if (i % 2 == 0) {
        evenNumber.push(i);
    }
}

console.log(evenNumber);
console.log(Array.isArray(evenNumber));


const oddNumber = [];

let i = 1
while (i <= 100) {
    if (i % 2 == 1) {
        oddNumber.push(i);
    }
    i++;
}
console.log(Array.isArray(oddNumber));

/**
 * Jangan hapus kode di bawah ini
 */

console.log(oddNumber);
module.exports = evenNumber;


const artistsAndSongs = {
    "Keyakizaka46": ["Silent Majority"],
    "Blackpink": ["How You Like That", "Ice Cream"],
    "JKT48": ["Rapsodi", "Heavy Rotation"],
    "Twice": ["What is Love?"],
}

artistsAndSongs["Babymetal"] = ["Gimme chocolate"];

delete artistsAndSongs["Keyakizaka46"];
console.log(artistsAndSongs)