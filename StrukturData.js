/**
 * Object
 * @type {{name: string, alamat: string}}
 *
 * Kali ini kita akan berkenalan dengan tipe data object. Sebuah tipe data yang sangat berguna dalam pengembangan
 * aplikasi dengan JavaScript. Object mampu menyimpan nilai dari beragam tipe data dan membentuk data yang lebih kompleks.
 *
 * Object berisi pasangan key dan value yang juga dikenal dengan property.
 * Key berperan mirip seperti nama variabel yang menyimpan sebuah nilai. Sementara,
 * value berisi nilai dengan tipe data apa pun termasuk objek lain. Key dan value di dalam
 * object dituliskan seperti berikut:
 */

const user = {name : "agas", alamat: "Depok"};
console.log(user);


/**
 * Key harus berupa string dan dituliskan sebelum titik dua (:), lalu diikuti dengan value-nya.
 * Meskipun key merupakan string, kita tidak perlu menuliskan tanda petik kecuali ada karakter khusus seperti spasi.
 *
 * Tanda koma pada properti terakhir bersifat opsional.
 * Namun, jika tanda koma tersebut ditulis akan lebih memudahkan ketika kita ingin memindah, mengubah, atau menghapus properti.
 *
 * Satu object dapat memiliki beberapa pasang key-value yang dipisahkan dengan tanda koma (,).
 *
 * Dalam menuliskan objek, baris baru tidaklah penting dan tidak akan berpengaruh apa pun.
 * Sehingga lebih baik setiap kita menetapkan key-value buatlah baris baru untuk memisahkan antar nilainya.
 * Hal ini akan memudahkan kita dalam membaca dan memahami struktur data dari sebuah object.
 *
 * Kemudian untuk mengakses nilai dari properti object,
 * kita dapat memanggil nama object lalu tanda titik dan diikuti nama propertinya. Contoh:
 *
 * const user = {
 *     firstName: "Luke",
 *     lastName: "Skywalker",
 *     age: 19,
 *     isJedi: true,
 * };
 *
 * console.log(`Halo, nama saya ${user.firstName} ${user.lastName}`);
 * console.log(`Umur saya ${user.age} tahun`);
 *
 */


/**
 * Selain dot operator, kita juga bisa mengakses properti dari object menggunakan bracket atau tanda kurung siku.
 *
 * user[“home world”];
 *
 * Untuk mengakses key yang memiliki spasi atau karakter khusus lainnya maka kita perlu menggunakan bracket seperti di atas.
 *
 * const user = {
 *     firstName: "Luke",
 *     lastName: "Skywalker",
 *     age: 19,
 *     isJedi: true,
 *     "home world": "Tattooine"
 * };
 * console.log(`Halo, nama saya ${user.firstName} ${user.lastName}`);
 * console.log(`Umur saya ${user.age} tahun`);
 * console.log(`Saya berasal dari ${user["home world"]}`);
 *
 */

/**
 * Setelah mempelajari bagaimana membuat object dan menampilkan property di dalamnya,
 * selanjutnya kita akan memodifikasi sebuah object.
 * Untuk mengubah nilai properti di dalam object kita gunakan assignment operator (=).
 *
 * const spaceship = {
 *     name: "Millenium Falcon",
 *     manufacturer: "Corellian Engineering Corporation",
 *     maxSpeed: 1200,
 *     color: "Light gray"
 * };
 *
 * spaceship.color = "Glossy red";
 * spaceship["maxSpeed"] = 1300;
 * console.log(spaceship);
 */

const spaceship = {
    name: "Millenium Falcon",
    manufacturer: "Corellian Engineering Corporation",
    maxSpeed: 1200,
    color: "Light gray"
};

spaceship.name = "Millenium Falcon bachott"
spaceship.color = "Merah Lebam";
spaceship["maxSpeed"] = 5000;
console.log(spaceship);

//spaceship = { name: "New Millenium Falcon" }; //will be error


const kapalUlangAlik = {
    nameKapal: "Millenium Falcon",
    namamanufacturer: "Madura Corp",
    maxSpeed: 1200,
    warnaKapal: "Light gray"
};

kapalUlangAlik.warnaKapal = "Glossy red";
kapalUlangAlik["maxSpeed"] = 1300;
kapalUlangAlik.class = "Light freighter";

console.log(kapalUlangAlik);

/* output
{
  name: 'Millenium Falcon',
  manufacturer: 'Corellian Engineering Corporation',
  maxSpeed: 1300,
  color: 'Glossy red',
  class: 'Light freighter'
}
*/


/**
 * Kita juga dapat menghapus property pada object menggunakan keyword delete seperti berikut:
 */

spaceship.color = "Glossy red";
spaceship["maxSpeed"] = 1300;

delete spaceship.manufacturer;

console.log(spaceship);

/* output
{ name: 'Millenium Falcon', maxSpeed: 1300, color: 'Glossy red' }
 */