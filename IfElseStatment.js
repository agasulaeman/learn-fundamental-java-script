/**
 * IF ELSE statment
 *
 * Statement if akan menguji suatu kondisi. Jika kondisi bernilai true, maka blok kode di dalamnya akan dijalankan.
 * Sebaliknya, jika bernilai false, maka proses yang ditentukan akan dilewatkan.
 */

/** Contoh logical IF
 *
 */

const isRaining = true;

console.log("Persiapan sebelum berangkat kegiatan.");
if (isRaining) {
    console.log("Hari ini hujan. Bawa payung.");
}
console.log("Berangkat kegiatan.");

/* output:
Persiapan sebelum berangkat kegiatan.
Hari ini hujan. Bawa payung.
Berangkat kegiatan.
 */

/**
 * contoh logical else
 */

let x = 50;

if(x > 70) {
    console.log(x);
} else {
    console.log("Nilai kurang dari 70");
}

/* output
Nilai kurang dari 70
*/

/**
 * contoh logical if else , else if
 */

let language = "French";
let greeting = "Selamat Pagi"

if(language === "English") {
    greeting = "Good Morning!";
} else if(language === "French") {
    greeting = "Bonjour!"
} else if(language === "Japanese") {
    greeting = "Ohayou Gozaimasu!"
}

console.log(greeting);

/* output
Bonjour!
*/


/**
 * Selain if statement di atas, JavaScript juga mendukung ternary operator atau conditional expressions.
 * Dengan ini, kita bisa menuliskan if-else statement hanya dalam satu baris.
 */

// condition ? true expression : false expression

const isMember = false;
const discount = isMember ? 0.1 : 0;
console.log(`Anda mendapatkan diskon sebesar ${discount * 100}%`)

/* output
Anda mendapatkan diskon sebesar 0%
 */

const bulanHariIni = false;
const date = bulanHariIni ? "january" : 10 ;
console.log(`date today ${date }`)


/**
 * Truthy & Falsy
 */

/**
 * Setiap nilai pada JavaScript pada dasarnya juga mewarisi sifat boolean.
 * Nilai ini dikenal dengan truthy atau falsy. Nilai truthy berarti nilai yang ketika dievaluasi akan
 * menghasilkan nilai true, begitu pula falsy bernilai false. Jadi manakah yang termasuk truthy dan falsy?
 * Selain nilai boolean false, tipe data atau nilai yang dianggap falsy, antara lain:
 *
 * Number 0
 * BigInt 0n
 * String kosong seperti “” atau ‘’
 * null
 * undefined
 * NaN, atau Not a Number
 */

/**
 * Contoh
 */

let name = "";

if (name) {
    console.log(`Halo, ${name}`);
} else {
    console.log("Nama masih kosong");
}

/* output:
Nama masih kosong
 */

/**
 * Switch Case Statement
 *
 * JavaScript juga mendukung switch statement untuk melakukan pengecekan banyak kondisi dengan lebih mudah dan ringkas.
 *
 * Contoh
 */

/**
 * base syntax
 *
 * switch (expression) {
 *     case value1:
 *         // do something
 *         break;
 *     case value2:
 *         // do something
 *         break;
 *     default:
 *     // do something else
 * }
 */


/**
 * contoh
 */


let bahasa = "French";
let salam = null;

switch (bahasa) {
    case "English":
        salam = "Good Morning!";
        break;
    case "French":
        salam = "Bonjour!";
        break;
    case "Japanese":
        salam = "Ohayou Gozaimasu!";
        break;
    default:
        salam = "Selamat Pagi!";
}

console.log(salam);

/* output
Bonjour!
*/
